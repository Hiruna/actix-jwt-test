use actix_files::Files;
use actix_web::{App, HttpResponse, HttpServer, middleware::Logger, web::{Query, resource}};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use jsonwebtoken::{decode, encode, Header, TokenData, Validation};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
struct Token {
    name: String
}

fn get_name(auth: BearerAuth) -> HttpResponse {
    let token = decode::<Token>(auth.token(), b"private", &Validation{validate_exp: false, ..Validation::default()});
    if let Ok(TokenData{ claims: Token{ name }, .. }) = token {
        return HttpResponse::Ok().body(format!("Hello, {}!", name))
    }
    HttpResponse::Unauthorized().reason("Improper JWT\n{:?}").finish()
    
}

#[derive(Deserialize)]
struct CreateRequest {
    name: String,
}

fn create_name(params: Query<CreateRequest>) -> String {
    let name = params.name.clone();
    encode(&Header::default(), &Token{name}, b"private")
        .unwrap_or("Error: Encoding the JWT".to_string())
}

fn main() {
    HttpServer::new(||{
        App::new()
            .wrap(Logger::default())
            .service(resource("/get").to(get_name))
            .service(resource("/create").to(create_name))
            .service(Files::new("/", "./src/static").index_file("index.html"))
    })
        .bind("127.0.0.1:8080").unwrap()
        .run().unwrap()
}
